const _ = require('lodash');
const mongoose = require('mongoose');
const City = mongoose.model('City');
const { handleError, handleSuccess } = require('../../config/response');

exports.list = async (req, res) => {
  City.find({}, function (err, cities) {
    if (err) {
      return handleError(res, 500, 'Có lỗi xảy ra, vui lòng thử lại');
    }
    return handleSuccess(res, 200, cities);
  });
}