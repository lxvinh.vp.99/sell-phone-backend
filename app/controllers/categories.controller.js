const _ = require('lodash');
const mongoose = require('mongoose');
const Category = mongoose.model('Category');
const config = require('../../config/env/all');
const { handleError, handleSuccess } = require('../../config/response');

exports.list = async (req, res) => {
  try {
    let categories = await Category.find().lean().exec();
    categories = categories.map((ctg) => {
      return {
        ...ctg,
        image: config.HOST + "/images/categories/" + ctg.sID + ".png",
      };
    });
    return handleSuccess(res, 200, categories);
  } catch (error) {
    console.log(error);
    return handleError(res, 500, 'Có lỗi xảy ra, vui lòng thử lại');
  }
}