const _ = require('lodash');
const mongoose = require('mongoose');
const Product = mongoose.model('Product');
const Image = mongoose.model('Image');
const WishList = mongoose.model('WishList');
const User = mongoose.model('User');
const City = mongoose.model('City');
const Category = mongoose.model('Category');
const { handleError, handleSuccess } = require('../../config/response');
const config = require('../../config/env/all');
const helpers = require('../helpers/helpers');
const bluebird = require('bluebird');
const multipleUploadMiddleware = require("../helpers/multipleUploadMiddleware");
const { isArray } = require('lodash');
const axios = require('axios');
const TLSSigAPIv2 = require('tls-sig-api-v2');

// đăng bán sản phẩm
exports.create = async (req, res) => {
    try {
        await multipleUploadMiddleware(req, res);
        const { sellerEmail, sellerFullName, sellerPhoneNumber, sellerAddress, title, description, price, category, city } = req.body;
        if (req.files.length <= 0) {
            throw new Error('Bạn phải chọn ít nhất 1 ảnh');
        }
        if (!sellerEmail || !sellerAddress || !sellerFullName || !sellerPhoneNumber || !title || !description || !price || !category || !city) {
            throw new Error('Vui lòng điền đầy đủ thông tin');
        }
        const user = req.user;
        const product = new Product({
            ...req.body,
            "sellerEmail": req.user.email,
            "seller": helpers.getObjectId(user),
            "status": 1
        });
        await product.save();
        let productId = helpers.getObjectId(product);
        for (let i = 0; i < req.files.length; i++) {
            const image = new Image({
                "filename": req.files[i].filename,
                "product": helpers.getObjectId(productId)
            });
            await image.save();
        }
        handleSuccess(res, 200, product, "Đăng bán sản phẩm thành công");
    } catch (error) {
        if (error.code === "LIMIT_UNEXPECTED_FILE") {
            return handleError(res, 400, `Exceeds the number of files allowed to upload.`);
        }
        return handleError(res, 400, error.message);
    }
};

exports.test = async (req, res) => {
    try {
        // await multipleUploadMiddleware(req, res);
        // console.log(req.files);
        console.log(req.body);
        return res.send(`Your files has been uploaded.`);
    } catch (error) {
        if (error.code === "LIMIT_UNEXPECTED_FILE") {
            return res.send(`Exceeds the number of files allowed to upload.`);
        }
        return res.send(`Error when trying upload many files: ${error}}`);
    }
};

exports.list = async (req, res) => {
    const { user } = req;
    const { page } = req.query;
    const { city: sIDCity, category: sIDCategory } = req.params;
    const pageNumber = Number(page);
    const sizeNumber = 10;
    const city = await City.findOne({ sID: `/${sIDCity}` }).lean().exec();
    const category = await Category.findOne({ sID: `${sIDCategory}` }).lean().exec();
    let condition = {
        status: 1,
    };
    let breadcrumb = [];
    if (city) {
        condition = {
            ...condition,
            city: city._id,
        }
        breadcrumb.push({
            name: city.name,
            sID: city.sID,
            url: `${city.name}/${category ? category.sID : 'mua-ban'}`,
            active: false,
        });
    } else {
        breadcrumb.push({
            name: 'Toàn quốc',
            sID: '/toan-quoc',
            url: `/toan-quoc/${category ? category.sID : 'mua-ban'}`,
            active: false,
        })
    };
    if (category) {
        condition = {
            ...condition,
            category: category._id,
        }
        breadcrumb.push({
            name: category.name,
            sID: category.sID,
            active: true,
        });
    } else {
        breadcrumb.push({
            name: 'Mua bán',
            sID: 'mua-ban',
            active: true,
        });
    };
    const totalProducts = await Product.find(condition).countDocuments().exec();
    const products = await Product.find(condition)
        .sort({ "createdAt": -1 })
        .skip((pageNumber - 1) * sizeNumber)
        .limit(sizeNumber)
        .populate('seller', 'email')
        .exec();
    let wishProducts = {};
    if (user) {
        const wishList = await WishList.aggregate()
            .match({
                user: helpers.getObjectId(user),
            })
            .limit(1)
            .unwind('$products');
        if (wishList.length) {
            wishProducts = _.reduce(
                wishList,
                (result, product) => {
                    result[helpers.getObjectId(product.products)] = true
                    return result
                },
                {}
            )
        }
    }
    let productsRes = [];
    for (let i = 0; i < products.length; i++) {
        let images = await Image.aggregate().match({ product: helpers.getObjectId(products[i]._id), });
        let linkImages = _.map(images, image => {
            return config.HOST + "/uploads/" + image.filename;
        });
        productsRes.push({
            title: products[i].title,
            description: products[i].description,
            price: products[i].price,
            _id: products[i]._id,
            images: linkImages,
            isOwned: req.user && products[i].seller && (req.user.email == products[i].seller.email),
            hasLiked: wishProducts[products[i]._id] || false,
            createdAt: products[i].createdAt,
            oldPrice: products[i].oldPrice,
            sellerAddress: products[i].sellerAddress,
        });
    }
    let data = {
        totalProducts: totalProducts,
        products: productsRes,
        breadcrumb,
    }
    return handleSuccess(res, 200, data);
}

exports.listTop = async (req, res) => {
    const { user } = req;
    const products = await Product.find({ "status": 1 })
        .sort({ "createdAt": -1 })
        .limit(10)
        .populate('seller', 'email')
        .exec();
    let wishProducts = {};
    if (user) {
        const wishList = await WishList.aggregate()
            .match({
                user: helpers.getObjectId(user),
            })
            .limit(1)
            .unwind('$products');
        if (wishList.length) {
            wishProducts = _.reduce(
                wishList,
                (result, product) => {
                    result[helpers.getObjectId(product.products)] = true
                    return result
                },
                {}
            )
        }
    }
    let productsRes = [];
    for (let i = 0; i < products.length; i++) {
        let images = await Image.aggregate().match({ product: helpers.getObjectId(products[i]._id), });
        let linkImages = _.map(images, image => {
            return config.HOST + "/uploads/" + image.filename;
        });
        productsRes.push({
            title: products[i].title,
            description: products[i].description,
            price: products[i].price,
            _id: products[i]._id,
            images: linkImages,
            isOwned: req.user && products[i].seller && (req.user.email == products[i].seller.email),
            hasLiked: wishProducts[products[i]._id] || false,
            createdAt: products[i].createdAt,
            oldPrice: products[i].oldPrice,
            sellerAddress: products[i].sellerAddress,
        });
    }
    let data = {
        title: 'Tin đăng mới',
        products: productsRes
    }
    return handleSuccess(res, 200, data);
}

exports.detail = async (req, res) => {
    let { id } = req.params;
    const { user } = req;
    if (mongoose.Types.ObjectId.isValid(id)) {
        const product = await Product.findOne({ _id: helpers.getObjectId(id), status: 1 });
        let data = {};
        if (product) {
            const wishListProducts = await WishList.findOne({ user: helpers.getObjectId(user) });
            let hasLiked = false;
            if (wishListProducts) {
                let index = _.findIndex(wishListProducts.products, function (o) { return o.toString() == product._id.toString(); });
                if (index != -1) {
                    hasLiked = true;
                }
            }
            let images = await Image.aggregate().match({ product: helpers.getObjectId(product._id), });
            let linkImages = _.map(images, image => {
                return config.HOST + "/uploads/" + image.filename;
            });
            let productRes = {
                title: product.title,
                description: product.description,
                price: product.price,
                _id: product._id,
                sellerEmail: product.sellerEmail,
                sellerAddress: product.sellerAddress,
                sellerFullName: product.sellerFullName,
                sellerPhoneNumber: product.sellerPhoneNumber,
                images: linkImages,
                createdAt: product.createdAt,
                oldPrice: product.oldPrice,
                hasLiked: hasLiked
            }
            data = {
                product: productRes
            }
        }
        return handleSuccess(res, 200, data, "Success");
    }
    else {
        return handleError(res, 404, "Không tìm thấy sản phẩm");
    }
}

exports.updateStatus = async (req, res) => {
    try {
        let { id, status } = req.body;
        if (mongoose.Types.ObjectId.isValid(id)) {
            const product = await Product.findOne({ _id: helpers.getObjectId(id), seller: helpers.getObjectId(req.user) });
            if (!product) {
                return handleError(res, 404, "Không tìm thấy sản phẩm");
            }
            if (product.status == 3) {
                throw new Error('Sản phẩm đã bán không thể cập nhật');
            }
            product.status = status;
            await product.save();
            return handleSuccess(res, 200, null, "Success");
        }
        else {
            return handleError(res, 404, "Không tìm thấy sản phẩm");
        }
    } catch (error) {
        return handleError(res, 400, error.message);
    }
}

exports.getMyProducts = async (req, res) => {
    try {
        const products = await Product.find({ seller: helpers.getObjectId(req.user) });
        return handleSuccess(res, 200, products, "Success");
    } catch (error) {
        return handleError(res, 400, error.message);
    }
}

exports.createGroupChatIM = async (req, res) => {
    const { user } = req;
    const { productId } = req.body;
    const product = await Product.findOne({ _id: helpers.getObjectId(productId) }).lean().exec();
    const sellerInfo = await User.findOne({ _id: helpers.getObjectId(product.seller) }).lean().exec();
    if (!product) {
        return handleError(res, 404, "Không tìm thấy sản phẩm");
    }
    if (product.seller.toString() === user._id.toString()) {
        return handleError(res, 400, "Sản phẩm này là của bạn");
    }
    const groupId = `${user.userId}-${product.productId}-${sellerInfo.userId}`;
    const api = new TLSSigAPIv2.Api(config.SDKAPPID, config.SECRETKEY_TENCENT);
    const sig = api.genSig(config.OPERATOR_ID, 86400 * 180);
    const params = {
        sdkappid: config.SDKAPPID,
        identifier: config.OPERATOR_ID,
        usersig: sig,
        random: Math.random(),
        contenttype: 'json',
    };
    axios({
        method: 'post',
        url: `${config.TENCENT_DOMAIN}/v4/group_open_http_svc/create_group`,
        data: {
            Owner_Account: user.userId.toString(),
            Type: "Private",
            Name: groupId,
            Introduction: product.title,
            GroupId: groupId,
            MemberList: [
                {
                    Member_Account: sellerInfo.userId.toString(),
                },
            ]
        },
        params,
    }).then((imRes) => {
        console.log(imRes.data);
        const { data } = imRes;
        if (data.ErrorCode !== 0) {
            return handleSuccess(res, 200, groupId  , "Success");
        }
        axios({
            method: 'post',
            url: `${config.TENCENT_DOMAIN}/v4/group_open_http_svc/send_group_msg`,
            data: {
                GroupId: data.GroupId,
                MsgBody: [
                    {
                        MsgType: "TIMTextElem",
                        MsgContent: {
                            Text: "Tạo phòng thành công"
                        }
                    },
                ],
            },
            params,
        }).then((imRes) => {
            return handleSuccess(res, 200, groupId, "Success");
        }).catch((e) => {
            return handleError(res, 400, 'Có lỗi xảy ra, vui lòng thử lại');
        })
    }).catch((e) => {
        return handleError(res, 400, 'Có lỗi xảy ra, vui lòng thử lại');
    })
}