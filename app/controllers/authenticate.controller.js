const _ = require('lodash');
const mongoose = require('mongoose');
const User = mongoose.model('User');
const jwt = require('jsonwebtoken');
const config = require('../../config/env/all');
const { handleError, handleSuccess } = require('../../config/response');
const e = require('express');
// const helper = require('../libs/helper');
/**
 * Check authenticate
 */
exports.isAuthenticated = function (req, res, next) {
    if (req.headers &&
        req.headers.authorization &&
        req.headers.authorization.split(' ')[0] === 'JWT') {
        var jwtToken = req.headers.authorization.split(' ')[1];
        jwt.verify(jwtToken, config.SECRETKEY, function (err, payload) {
            if (err) {
                return handleError(res, 401, 'Unauthorized user!');
            } else {
                User.findOne({
                    'email': payload.email
                }, function (err, user) {
                    if (user) {
                        user.password = undefined;
                        user.resetPasswordToken = undefined;
                        user.resetPasswordExpires = undefined;
                        req.user = user;
                        next();
                    } else {
                        return handleError(res, 401, 'Unauthorized user!');
                    }
                });
            }
        });
    } else {
        return handleError(res, 401, 'Unauthorized user!');
    }
};
exports.getUserInfo = function (req, res, next) {
    if (req.headers &&
        req.headers.authorization &&
        req.headers.authorization.split(' ')[0] === 'JWT') {
        var jwtToken = req.headers.authorization.split(' ')[1];
        jwt.verify(jwtToken, config.SECRETKEY, function (err, payload) {
            if (payload) {
                User.findOne({
                    'email': payload.email
                }, function (err, user) {
                    if (user) {
                        user.password = undefined;
                        user.resetPasswordToken = undefined;
                        user.resetPasswordExpires = undefined;
                        req.user = user;
                    }
                    next();
                });
            } else {
                next();
            }
        });
    }
}
exports.verifyTokenReset = async (req, res, next, token) => {
    try {
        const user = await User.findOne({
            resetPasswordToken: token,
            resetPasswordExpires: { $gt: Date.now() },
        });
        if (!user) {
            return next(
                new Error(
                    'ResetPasswordToken của bạn không hợp lệ hoặc đã hết hạn'
                )
            );
        };
        req.user = user;
        next();
    } catch (error) {
        next(new Error(error.message));
    }
}