const _ = require('lodash');
const mongoose = require('mongoose');
const Product = mongoose.model('Product');
const LiveStream = mongoose.model('LiveStream');
const Image = mongoose.model('Image');
const { handleError, handleSuccess } = require('../../config/response');
const config = require('../../config/env/all');
const helpers = require('../helpers/helpers');
const bluebird = require('bluebird');
const multipleUploadMiddleware = require("../helpers/multipleUploadMiddleware");
const { isArray } = require('lodash');
const axios = require('axios');
const TLSSigAPIv2 = require('tls-sig-api-v2');

// tao live
exports.create = async (req, res) => {
  try {
    const user = req.user;
    const { productId } = req.body;
    await LiveStream.updateMany({ status: 1, user: user._id }, { status: 2 });
    const product = await Product.findOne({ _id: helpers.getObjectId(productId) }).exec();
    const liveStream = new LiveStream({
      "product": helpers.getObjectId(productId),
      "user": helpers.getObjectId(product.seller),
    });
    await liveStream.save();
    const groupId = liveStream._id;
    const api = new TLSSigAPIv2.Api(config.SDKAPPID, config.SECRETKEY_TENCENT);
    const sig = api.genSig(config.OPERATOR_ID, 86400 * 180);
    const params = {
      sdkappid: config.SDKAPPID,
      identifier: config.OPERATOR_ID,
      usersig: sig,
      random: Math.random(),
      contenttype: 'json',
    };
    axios({
      method: 'post',
      url: `${config.TENCENT_DOMAIN}/v4/group_open_http_svc/create_group`,
      data: {
        Owner_Account: user.userId.toString(),
        Type: "AVChatRoom",
        Name: groupId,
        GroupId: groupId,
      },
      params,
    }).then((imRes) => {
      console.log(imRes.data);
      const { data } = imRes;
      if (data.ErrorCode !== 0) {
        return handleError(res, 400, "Tạo phòng chat không thành công");
      } else {
        return handleSuccess(res, 200, liveStream._id, "Tạo livestream thành công");
      }
    }).catch((e) => {
      return handleError(res, 400, 'Có lỗi xảy ra, vui lòng thử lại');
    })
  } catch (error) {
    console.log(error);
    return handleError(res, 400, 'Có lỗi xảy ra, vui lòng thử lại');
  }
};

exports.detail = async (req, res) => {
  const { user } = req;
  let { id: liveStreamId } = req.params;
  if (liveStreamId === 'undefined' || !liveStreamId) return handleError(res, 400, 'Có lỗi xảy ra, vui lòng thử lại');
  const liveStream = await LiveStream.findOne({ _id: helpers.getObjectId(liveStreamId), status: 1 })
    .populate({
      path: 'product',
    })
    .lean()
    .exec();
  return handleSuccess(res, 200, liveStream);
}

exports.endRoom = async (req, res) => {
  const { user } = req;
  const { roomID } = req.body;
  if (!roomID) return handleError(res, 400, 'Có lỗi xảy ra, vui lòng thử lại');
  await LiveStream.findOneAndUpdate({ _id: helpers.getObjectId(roomID), status: 1, user: user._id }, { status: 2 });
  return handleSuccess(res, 200, null, 'End room thành công');
}

exports.list = async (req, res) => {
  const liveStreams = await LiveStream.find({ status: 1 })
    .populate('product')
    .lean()
    .exec();
  const data = await Promise.all(liveStreams.map(async (live) => {
    let images = await Image.aggregate().match({ product: helpers.getObjectId(live.product._id), });
    let linkImages = _.map(images, image => {
      return config.HOST + "/uploads/" + image.filename;
    });
    return {
      ...live,
      thumnails: linkImages,
    }
  }));
  return handleSuccess(res, 200, data);
}