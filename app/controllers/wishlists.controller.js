const _ = require('lodash');
const mongoose = require('mongoose');
const Product = mongoose.model('Product');
const Image = mongoose.model('Image');
const WishList = mongoose.model('WishList');
const { handleError, handleSuccess } = require('../../config/response');
const config = require('../../config/env/all');
const helpers = require('../helpers/helpers');

exports.add = async (req, res) => {
    try {
        const user = req.user;
        const { productId } = req.body;
        const wishList = await WishList.findOne({ user: helpers.getObjectId(user) });
        wishList.products = wishList.products || [];
        wishList.products = _.uniqWith(
            _.compact(_.concat(wishList.products, [productId])),
            helpers.isEquals
        );
        await wishList.save();
        handleSuccess(res, 200, null, 'OK');
    } catch (error) {
        console.log(error);
        return handleError(res, 400, error.message);
    }
};
exports.remove = async (req, res) => {
    try {
        const user = req.user;
        const { productId } = req.body;
        const wishList = await WishList.findOne({ user: helpers.getObjectId(user) });
        wishList.products = _.reject(wishList.products, wishPoduct =>
            helpers.isEquals(wishPoduct, productId)
        );
        await wishList.save();
        handleSuccess(res, 200, null, 'OK');
    } catch (error) {
        return handleError(res, 400, error.message);
    }
};

exports.getWishList = async (req, res) => {
    try {
        let user = req.user;
        const wishListProducts = await WishList.findOne({ user: helpers.getObjectId(user) })
            .populate('products')
            .exec();
        let productsRes = [];
        for (let i = 0; i < wishListProducts.products.length; i++) {
            let images = await Image.aggregate().match({ product: helpers.getObjectId(wishListProducts.products[i]._id), }).limit(1);
            let linkImages = _.map(images, image => {
                return config.HOST + "/uploads/" + image.filename;
            });
            productsRes.push({
                title: wishListProducts.products[i].title,
                // description: products[i].description,
                price: wishListProducts.products[i].price,
                _id: wishListProducts.products[i]._id,
                images: linkImages,
                oldPrice: wishListProducts.products[i].oldPrice,
                isOwned: req.user && (req.user.email == wishListProducts.products[i].seller.email)
            });
        }
        return handleSuccess(res, 200, productsRes, 'OK');
    } catch (error) {
        console.log(error);
        return handleError(res, 400, error.message);
    }
}