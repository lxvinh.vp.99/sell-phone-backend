const liveStream = require('../controllers/livestreams.controller');
const user = require('../controllers/users.controller');
module.exports = app => {
    app.route('/livestreams/create').post(user.isAuthenticated, liveStream.create);
    app.route('/livestreams/end').post(user.isAuthenticated, liveStream.endRoom);
    app.route('/livestreams/list').post(liveStream.list);
    app.route('/livestreams/detail/:id').post(liveStream.detail);
}
