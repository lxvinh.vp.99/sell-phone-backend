const category = require('../controllers/categories.controller');
module.exports = app => {
  app.route('/category/list').get(category.list);
}
