const user = require('../controllers/users.controller');
module.exports = app => {
    app.route('/users/create').post(user.create);
    app.route('/users/login').post(user.login);
    app.route('/users/myinfo').post(user.isAuthenticated, user.profile);
    app.route('/users/changepassword').post(user.isAuthenticated, user.changePassword);
    app.route('/users/editprofile').post(user.isAuthenticated, user.editprofile);
    app.route('/users/forgotpassword').post(user.forgotpassword);
    app.route('/users/verify/:tokenreset').post(user.verify);
    app.route('/users/resetpassword/:tokenreset').post(user.resetpassword);
    app.param('tokenreset', user.verifyTokenReset);
}
