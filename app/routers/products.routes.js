const product = require('../controllers/products.controller');
const user = require('../controllers/users.controller');
module.exports = app => {
    app.route('/products/create').post(user.isAuthenticated, product.create);
    app.route('/products/list/:city/:category').post(user.getUserInfo, product.list);
    app.route('/products/list-top').post(user.getUserInfo, product.listTop);
    app.route('/products/detail/:id').post(user.getUserInfo, product.detail);
    app.route('/products/updatestatus').post(user.isAuthenticated, product.updateStatus);
    app.route('/products/myproducts').post(user.isAuthenticated, product.getMyProducts);
    app.route('/create-chat').post(user.isAuthenticated, product.createGroupChatIM);
}
