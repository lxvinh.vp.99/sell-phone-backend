const wishList = require('../controllers/wishlists.controller');
const user = require('../controllers/users.controller');
module.exports = app => {
    app.route('/wishlists/add').post(user.isAuthenticated, wishList.add);
    app.route('/wishlists/getwishlist').post(user.isAuthenticated, wishList.getWishList);
    app.route('/wishlists/remove').post(user.isAuthenticated, wishList.remove);
}
