const city = require('../controllers/cities.controller');
module.exports = app => {
  app.route('/city/list').get(city.list);
}
