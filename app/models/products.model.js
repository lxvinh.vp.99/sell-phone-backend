const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
const Schema = mongoose.Schema;
const productSchema = new Schema(
    {
        "title": {
            type: String
        },
        "description": {
            type: String
        },
        "price": {
            type: Number
        },
        "category": {
            type: Schema.ObjectId,
            ref: 'Category',
        },
        "city": {
            type: Schema.ObjectId,
            ref: 'City',
        },
        "seller": {
            type: Schema.ObjectId,
            ref: 'User',
        },
        "sellerEmail": {
            type: String
        },
        "sellerFullName": {
            type: String
        },
        "sellerPhoneNumber": {
            type: String
        },
        "sellerAddress": {
            type: String
        },
        "status": {
            type: Number,
            enum: [1, 2, 3], // 1: Chưa bán, 2: Đang giao hàng, 3: Đã bán
            default: 1,
        }
    },
    {
        usePushEach: true,
        timestamps: true,
    }
);

productSchema.plugin(autoIncrement.plugin, { model: 'Product', field: 'productId', startAt: 1 });
mongoose.model('Product', productSchema);
