const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const _ = require('lodash');
const wishListSchema = new Schema(
    {
        user: {
            type: Schema.ObjectId,
            ref: 'User',
        },
        products: [
            {
                type: Schema.ObjectId,
                ref: 'Product',
            }
        ]
    },
    {
        usePushEach: true,
        timestamps: true,
    }
)

mongoose.model('WishList', wishListSchema);
