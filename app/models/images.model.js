const mongoose = require('mongoose');
// const helper = require('../libs/helper');
const Schema = mongoose.Schema;
// const _ = require('lodash');
const imageSchema = new Schema(
    {
        "filename": {
            type: String
        },
        "product": {
            type: Schema.ObjectId,
            ref: 'Product',
        }
    },
    {
        usePushEach: true,
        timestamps: true,
    }
);
mongoose.model('Image', imageSchema);
