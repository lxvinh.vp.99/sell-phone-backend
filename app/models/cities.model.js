const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const citySchema = new Schema(
  {
    "sID": {
      type: String
    },
    "name": {
      type: String
    }
  },
  {
    usePushEach: true,
    timestamps: true,
  }
);
mongoose.model('City', citySchema);
