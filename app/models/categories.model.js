const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const categorySchema = new Schema(
  {
    "sID": {
      type: String
    },
    "name": {
      type: String
    }
  },
  {
    usePushEach: true,
    timestamps: true,
  }
);
mongoose.model('Category', categorySchema);
