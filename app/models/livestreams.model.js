const mongoose = require('mongoose');
const autoIncrement = require('mongoose-auto-increment');
// const helper = require('../libs/helper');
const Schema = mongoose.Schema;
// const _ = require('lodash');
const liveStreamSchema = new Schema(
  {
    "user": {
      type: Schema.ObjectId,
      ref: 'User',
    },
    "status": {
      type: Number,
      enum: [1, 2], // 1: Đang live, 2: Đã kết thúc 
      default: 1,
    },
    "product": {
      type: Schema.ObjectId,
      ref: 'Product',
    }
  },
  {
    usePushEach: true,
    timestamps: true,
  }
);

liveStreamSchema.plugin(autoIncrement.plugin, { model: 'LiveStream', field: 'liveStreamId', startAt: 1 });
const LiveStream = mongoose.model('LiveStream', liveStreamSchema);
module.exports = LiveStream;

