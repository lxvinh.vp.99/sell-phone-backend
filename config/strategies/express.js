const express = require('express');
const _ = require('lodash');
const config = require('../config');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const cors = require('cors');
// const helmet = require('helmet');
const { handleError } = require('../response');
const root = require('../root');
const fileupload = require("express-fileupload");
module.exports = function (db) {
  const server = express();
  // Load all model
  const modelFiles = config.loadModels();
  _.forEach(modelFiles, file => {
    const combinedFilePath = file.replace('', '../../');
    require(combinedFilePath);
  });
  server.use(cors());
  // server.use(helmet());
  server.use(bodyParser.json());
  server.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
  // Middleware morgan
  if (process.env.NODE_ENV !== 'test') {
    server.use(morgan('dev'));
  }
  server.use('/uploads', express.static(root('uploads')));
  server.use('/images', express.static(root('images')));
  // Load all router
  const routerFiles = config.loadRouters();
  _.forEach(routerFiles, file => {
    const combinedFilePath = file.replace('', '../../');
    require(combinedFilePath)(server);
  });

  server.use(function (err, req, res, next) {
    if (err) {
      return handleError(res, 400, err.message);
    }
    next();
  });
  return server;
}
