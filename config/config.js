const glob = require("glob");
exports.loadRouters = () => {
  const files = glob.sync("app/routers/**/*.js", {
    ignore: "**/node_modules/**"
  });
  return files;
};
exports.loadModels = () => {
  const files = glob.sync("app/models/**/*.js", {
    ignore: "**/node_modules/**"
  });
  return files;
};
