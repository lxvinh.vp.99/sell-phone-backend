require('dotenv').config();
const _ = require('lodash');
module.exports = _.assignIn(
  {
    ENV: process.env.NODE_ENV || "development",
    PORT: process.env.PORT || 3000,
    SECRETKEY: process.env.SECRETKEY,
    HOST: process.env.HOST || `http://localhost:${process.env.PORT}`,
    // HOST: process.env.HOST || `http://192.168.100.102:${process.env.PORT}`,
    FRONT_HOST: process.env.FRONT_HOST,
    TENCENT_DOMAIN: process.env.TENCENT_DOMAIN || 'https://console.tim.qq.com',
    SDKAPPID: 1400499407,
    SECRETKEY_TENCENT: '7f8a820b7318845499834b0145d04d938cd45133d28e827968bff5d7f92bf44b',
    OPERATOR_ID: 'administrator',
  },
  require('./development') || {}
)
