const { DB, PORT, ENV } = require('./config/env/all');
const chalk = require('chalk');
const autoIncrement = require('mongoose-auto-increment');
function runBasicStaff() {
    const mongoose = require('mongoose');
    mongoose.Promise = global.Promise;
    mongoose.set('useUnifiedTopology', true);
    mongoose.set('useNewUrlParser', true);
    mongoose.set('useFindAndModify', false);
    mongoose.set('useCreateIndex', true);
    autoIncrement.initialize(mongoose.connection);
    const db = mongoose
        .connect(DB)
        .then(() => {
            console.log('%s MongoDB Connected', chalk.green('✓'));
            // require('./app/libs/common').init();
        })
        .catch(err => {
            console.log('%s Connect MongoDB Fail'), chalk.red('X');
        })
    return db
}
const db = runBasicStaff();
const app = require('./config/strategies/express')(db);
app.listen(PORT, () => {
    console.log(`%s Server listen on port ${PORT} in ${ENV}`, chalk.green('✓'));
})
module.exports = app;
